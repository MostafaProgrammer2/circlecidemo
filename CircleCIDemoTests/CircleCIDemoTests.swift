//
//  CircleCIDemoTests.swift
//  CircleCIDemoTests
//
//  Created by mustafa salah eldin on 11/27/18.
//  Copyright © 2018 AllahAkber. All rights reserved.
//

import XCTest
@testable import CircleCIDemo

class CircleCIDemoTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        let a = ViewController().returnOne()
        XCTAssertEqual(a, 2)
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
